#ifndef UTILS_H__
#define UTILS_H__ 1

#define MAX_PROBES			7
#define MAX_STATS			9

#define KMALLOC_COUNTER			0
#define KFREE_COUNTER			1
#define KMALLOC_MEM_COUNTER		2
#define KFREE_MEM_COUNTER		3
#define SCHEDULE_COUNTER		4
#define UP_COUNTER			5
#define DOWN_INTER_COUNTER		6
#define MUTEX_LOCK_COUNTER		7
#define MUTEX_UNLOCK_COUNTER		8

struct vm_area {
	pid_t pid;
	unsigned long addr;
	int size;
	struct list_head next_vm_area;
};

struct proc {
	pid_t pid;
	char comm[TASK_COMM_LEN];
	int tracer_stats[MAX_STATS];
	struct list_head next_proc;
};

#endif /* UTILS_H__ */

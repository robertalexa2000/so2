#ifndef KRETPROBE_H__
#define KRETPROBE_H__ 1

#include "utils.h"

#define MAX_ACTIVE	64

struct kretprobe probes[MAX_PROBES];

static int __kmalloc_handler(struct kretprobe_instance *ri, struct pt_regs *regs);
static int __kmalloc_entry_handler(struct kretprobe_instance *ri, struct pt_regs *regs);
static int kfree_entry_handler(struct kretprobe_instance *ri, struct pt_regs *regs);

static int schedule_entry_handler(struct kretprobe_instance *ri, struct pt_regs *regs);

static int up_entry_handler(struct kretprobe_instance *ri, struct pt_regs *regs);
static int down_inter_entry_handler(struct kretprobe_instance *ri, struct pt_regs *regs);

static int mutex_lock_entry_handler(struct kretprobe_instance *ri, struct pt_regs *regs);
static int mutex_unlock_entry_handler(struct kretprobe_instance *ri, struct pt_regs *regs);

#endif /* TRACER_H_ */

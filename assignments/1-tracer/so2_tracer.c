#include <linux/module.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/types.h>
#include <linux/miscdevice.h>
#include <linux/proc_fs.h>
#include <linux/seq_file.h>
#include <linux/kprobes.h>
#include <linux/dev_printk.h>
#include <linux/slab.h>
#include <linux/sched.h>

#include "so2_tracer.h"

/* probes */
extern struct kretprobe probes[MAX_PROBES];

/* procfs entry */
struct proc_dir_entry *proc_entry;
const struct proc_ops pops = {
	.proc_open = tracer_open,
	.proc_read = seq_read,
	.proc_release = tracer_release,
};

/* misc_dev */
const struct file_operations fops = {
	.unlocked_ioctl = tracer_ioctl,
};
struct miscdevice misc_dev = {
	.minor = TRACER_DEV_MINOR,
	.name = TRACER_DEV_NAME,
	.fops = &fops,
};

/* PID list */
LIST_HEAD(head);
LIST_HEAD(head_vm);

static int tracer_open(struct inode *inode, struct file *file)
{
	return single_open(file, tracer_show, NULL);
}

static int tracer_show(struct seq_file *s, void *v)
{
	struct list_head *curr, *curr_vm, *tmp, *tmp_vm;
	struct task_struct *task;
	struct vm_area *vm_area;
	struct pid *pid_struct;
	struct proc *proc;
	pid_t pid;
	int i;

	seq_printf(s, "%s %10s %10s %10s %10s %10s %10s %10s %10s %10s\n",
		   "PID", "kmalloc", "kfree", "kmalloc_mem", "kfree_mem",
		   "sched", "up", "down", "lock", "unlock");
	list_for_each_safe(curr, tmp, &head) {
		proc = container_of(curr, struct proc, next_proc);

		/* Cleanup dead processes */
		pid = proc->pid;
		pid_struct = find_get_pid(pid);
		task = pid_task(pid_struct, PIDTYPE_PID);

		if (!task || strncmp(task->comm, proc->comm, TASK_COMM_LEN)) {
			list_del(curr);
			kfree(proc);

			list_for_each_safe(curr_vm, tmp_vm, &head_vm) {
				vm_area = container_of(curr_vm, struct vm_area, next_vm_area);
				if (vm_area->pid == pid) {
					list_del(curr_vm);
					kfree(vm_area);
				}
			}

			continue;
		}

		/* Print stats */
		seq_printf(s, "%d", proc->pid);
		for (i = 0; i < MAX_STATS; i++)
			seq_printf(s, " %10d", proc->tracer_stats[i]);
		seq_printf(s, "\n");
	}

	return 0;
}

static int tracer_release(struct inode *inode, struct file *file)
{
	return single_release(inode, file);
}

static long tracer_ioctl(struct file *file, unsigned int cmd, unsigned long arg)
{
	struct list_head *curr, *tmp;
	struct proc *proc, *new_proc;
	struct task_struct *task;
	struct vm_area *vm_area;
	struct pid *pid_struct;
	int err;

	switch (cmd) {
	case TRACER_ADD_PROCESS:
		new_proc = kzalloc(sizeof(struct proc), GFP_KERNEL);
		if (!new_proc)
			return -ENOMEM;

		new_proc->pid = (pid_t) arg;

		pid_struct = find_get_pid(new_proc->pid);
		task = pid_task(pid_struct, PIDTYPE_PID);
		strncpy(new_proc->comm, task->comm, TASK_COMM_LEN);

		list_add(&new_proc->next_proc, &head);
		break;
	case TRACER_REMOVE_PROCESS:
		err = -ESRCH;

		list_for_each_safe(curr, tmp, &head) {
			proc = container_of(curr, struct proc, next_proc);
			if (proc->pid == (pid_t) arg) {
				err = 0;
				list_del(curr);
				kfree(proc);
				break;
			}
		}

		list_for_each_safe(curr, tmp, &head_vm) {
			vm_area = container_of(curr, struct vm_area, next_vm_area);
			if (vm_area->pid == (pid_t) arg) {
				list_del(curr);
				kfree(vm_area);
			}
		}

		return err;
	default:
		return -EINVAL;
	}

	return 0;
}

static int __init tracer_init(void)
{
	int i, err = 0;

	err = misc_register(&misc_dev);
	if (err) {
		pr_err("misc_register(err = %d)\n", err);
		goto out_register;
	}

	proc_entry = proc_create(TRACER_DEV_NAME, 0000, NULL, &pops);
	if (!proc_entry) {
		err = -ENOMEM;
		pr_err("proc_create()\n");
		goto out_proc_create;
	}

	for (i = 0; i < MAX_PROBES; i++) {
		err = register_kretprobe(&probes[i]);
		if (err) {
			dev_err(misc_dev.this_device,
				"register_kretprobe[%d] (err = %d)\n",
				i, err);
			goto out_register_kretprobe;
		}
	}

	return 0;

out_register_kretprobe:
	for (; i >= 0; i--)
		unregister_kretprobe(&probes[i]);
out_proc_create:
	proc_remove(proc_entry);
out_register:
	misc_deregister(&misc_dev);
	return err;
}

static void __exit tracer_exit(void)
{
	struct list_head *curr, *tmp;
	struct vm_area *vm_area;
	struct proc *proc;
	int i;

	misc_deregister(&misc_dev);
	proc_remove(proc_entry);
	for (i = 0; i < MAX_PROBES; i++)
		unregister_kretprobe(&probes[i]);

	list_for_each_safe(curr, tmp, &head) {
		proc = container_of(curr, struct proc, next_proc);
		list_del(curr);
		kfree(proc);
	}

	list_for_each_safe(curr, tmp, &head_vm) {
		vm_area = container_of(curr, struct vm_area, next_vm_area);
		list_del(curr);
		kfree(vm_area);
	}
}

module_init(tracer_init);
module_exit(tracer_exit);

MODULE_DESCRIPTION("Kprobe-based tracer");
MODULE_AUTHOR("Robert-Ionut Alexa robertalexa2000@gmail.com");
MODULE_LICENSE("GPL v2");

/*
 * SO2 kprobe based tracer header file
 *
 * this is shared with user space
 */

#ifndef TRACER_H__
#define TRACER_H__ 1

#include <asm/ioctl.h>
#ifndef __KERNEL__
#include <sys/types.h>
#endif /* __KERNEL__ */
#include <linux/kprobes.h>
#include <linux/ptrace.h>

#include "utils.h"

#define TRACER_DEV_MINOR 42
#define TRACER_DEV_NAME "tracer"

#define TRACER_ADD_PROCESS	_IOW(_IOC_WRITE, 42, pid_t)
#define TRACER_REMOVE_PROCESS	_IOW(_IOC_WRITE, 43, pid_t)

static int tracer_open(struct inode *inode, struct file *file);
static int tracer_show(struct seq_file *s, void *v);
static int tracer_release(struct inode *inode, struct file *file);

static long tracer_ioctl(struct file *file, unsigned int cmd, unsigned long arg);

#endif /* TRACER_H_ */

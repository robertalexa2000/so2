#include <linux/kprobes.h>
#include <linux/list.h>
#include <linux/slab.h>

#include "kretprobe.h"

extern struct list_head head;
extern struct list_head head_vm;

struct kretprobe probes[MAX_PROBES] = {
	{
		.kp.symbol_name = "__kmalloc",
		.handler = __kmalloc_handler,
		.entry_handler = __kmalloc_entry_handler,
		.maxactive = MAX_ACTIVE,
	},

	{
		.kp.symbol_name = "kfree",
		.entry_handler = kfree_entry_handler,
		.maxactive = MAX_ACTIVE,
	},

	{
		.kp.symbol_name = "schedule",
		.entry_handler = schedule_entry_handler,
		.maxactive = MAX_ACTIVE,
	},

	{
		.kp.symbol_name = "up",
		.entry_handler = up_entry_handler,
		.maxactive = MAX_ACTIVE,
	},

	{
		.kp.symbol_name = "down_interruptible",
		.entry_handler = down_inter_entry_handler,
		.maxactive = MAX_ACTIVE,
	},

	{
		.kp.symbol_name = "mutex_lock_nested",
		.entry_handler = mutex_lock_entry_handler,
		.maxactive = MAX_ACTIVE,
	},

	{
		.kp.symbol_name = "mutex_unlock",
		.entry_handler = mutex_unlock_entry_handler,
		.maxactive = MAX_ACTIVE,
	},
};

static int __kmalloc_handler(struct kretprobe_instance *ri, struct pt_regs *regs)
{
	pid_t pid = current->pid;
	struct vm_area *vm_area;
	struct list_head *curr;

	list_for_each(curr, &head_vm) {
		vm_area = container_of(curr, struct vm_area, next_vm_area);
		if (vm_area->pid == pid) {
			vm_area->addr = regs->ax;
			break;
		}
	}

	return 0;
}
NOKPROBE_SYMBOL(__kmalloc_handler);

static int __kmalloc_entry_handler(struct kretprobe_instance *ri, struct pt_regs *regs)
{
	pid_t pid = current->pid;
	struct vm_area *vm_area;
	struct list_head *curr;
	struct proc *proc;

	list_for_each(curr, &head) {
		proc = container_of(curr, struct proc, next_proc);
		if (proc->pid == pid) {
			proc->tracer_stats[KMALLOC_COUNTER]++;
			proc->tracer_stats[KMALLOC_MEM_COUNTER] += (int) regs->ax;

			vm_area = kzalloc(sizeof(struct vm_area), GFP_ATOMIC);
			if (!vm_area)
				return -ENOMEM;
			vm_area->pid = pid;
			vm_area->size = (int) regs->ax;
			list_add(&vm_area->next_vm_area, &head_vm);

			break;
		}
	}

	return 0;
}
NOKPROBE_SYMBOL(__kmalloc_entry_handler);

static int kfree_entry_handler(struct kretprobe_instance *ri, struct pt_regs *regs)
{
	struct list_head *curr, *tmp;
	pid_t pid = current->pid;
	struct vm_area *vm_area;
	struct proc *proc;

	list_for_each(curr, &head) {
		proc = container_of(curr, struct proc, next_proc);
		if (proc->pid == pid) {
			proc->tracer_stats[KFREE_COUNTER]++;
			break;
		}
	}

	list_for_each_safe(curr, tmp, &head_vm) {
		vm_area = container_of(curr, struct vm_area, next_vm_area);
		if (vm_area->pid == pid && vm_area->addr == regs->ax) {
			proc->tracer_stats[KFREE_MEM_COUNTER] += vm_area->size;
			list_del(curr);
			kfree(vm_area);
			break;
		}
	}

	return 0;
}
NOKPROBE_SYMBOL(kfree_entry_handler);

static int schedule_entry_handler(struct kretprobe_instance *ri, struct pt_regs *regs)
{
	pid_t pid = current->pid;
	struct list_head *curr;
	struct proc *proc;

	list_for_each(curr, &head) {
		proc = container_of(curr, struct proc, next_proc);
		if (proc->pid == pid) {
			proc->tracer_stats[SCHEDULE_COUNTER]++;
			break;
		}
	}

	return 0;
}
NOKPROBE_SYMBOL(schedule_entry_handler);


static int up_entry_handler(struct kretprobe_instance *ri, struct pt_regs *regs)
{
	pid_t pid = current->pid;
	struct list_head *curr;
	struct proc *proc;

	list_for_each(curr, &head) {
		proc = container_of(curr, struct proc, next_proc);
		if (proc->pid == pid) {
			proc->tracer_stats[UP_COUNTER]++;
			break;
		}
	}

	return 0;
}
NOKPROBE_SYMBOL(up_entry_handler);

static int down_inter_entry_handler(struct kretprobe_instance *ri, struct pt_regs *regs)
{
	pid_t pid = current->pid;
	struct list_head *curr;
	struct proc *proc;

	list_for_each(curr, &head) {
		proc = container_of(curr, struct proc, next_proc);
		if (proc->pid == pid) {
			proc->tracer_stats[DOWN_INTER_COUNTER]++;
			break;
		}
	}

	return 0;
}
NOKPROBE_SYMBOL(down_inter_entry_handler);

static int mutex_lock_entry_handler(struct kretprobe_instance *ri, struct pt_regs *regs)
{
	pid_t pid = current->pid;
	struct list_head *curr;
	struct proc *proc;

	list_for_each(curr, &head) {
		proc = container_of(curr, struct proc, next_proc);
		if (proc->pid == pid) {
			proc->tracer_stats[MUTEX_LOCK_COUNTER]++;
			break;
		}
	}

	return 0;
}
NOKPROBE_SYMBOL(mutex_lock_entry_handler);

static int mutex_unlock_entry_handler(struct kretprobe_instance *ri, struct pt_regs *regs)
{
	pid_t pid = current->pid;
	struct list_head *curr;
	struct proc *proc;

	list_for_each(curr, &head) {
		proc = container_of(curr, struct proc, next_proc);
		if (proc->pid == pid) {
			proc->tracer_stats[MUTEX_UNLOCK_COUNTER]++;
			break;
		}
	}

	return 0;
}
NOKPROBE_SYMBOL(mutex_unlock_entry_handler);

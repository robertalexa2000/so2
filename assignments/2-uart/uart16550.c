#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/moduleparam.h>
#include <linux/compiler_attributes.h>
#include <linux/device/class.h>
#include <linux/cdev.h>
#include <linux/fs.h>
#include <linux/device.h>
#include <linux/ioport.h>
#include <linux/interrupt.h>
#include <linux/kfifo.h>
#include <linux/wait.h>
#include <linux/slab.h>
#include <linux/io.h>

#include "uart16550.h"
#include "utils.h"

static int major = UART16550_MAJOR;
static int option = OPTION_BOTH;
static struct class *class;

module_param(major, int, 0);
MODULE_PARM_DESC(major, "The major assigned to the UART devices");
module_param(option, int, 0);
MODULE_PARM_DESC(option, "Selects what devices will be used - OPTION_COM1 (1), OPTION_COM2 (2), OPTION_BOTH (3)");

struct uart16550 {
	struct cdev cdev;
	struct class *class;
	struct device *device;
	u16 base_addr;
	spinlock_t lock;
	DECLARE_KFIFO(read_buff, u8, FIFO_SIZE);
	DECLARE_KFIFO(write_buff, u8, FIFO_SIZE);
	struct wait_queue_head wq_head;
} uart16550[MAX_NUMBER_DEVICES] = {
	{
		.base_addr = UART16550_COM1_ADDR,
	},
	{
		.base_addr = UART16550_COM2_ADDR,
	},
};

static int uart16550_com1_open(struct inode *inode, struct file *file)
{
	file->private_data = &uart16550[UART16550_COM1_MINOR];
	return 0;
}

static int uart16550_com2_open(struct inode *inode, struct file *file)
{
	file->private_data = &uart16550[UART16550_COM2_MINOR];
	return 0;
}

static ssize_t uart16550_read(struct file *file, char __user *buf, size_t size, loff_t *offset)
{
	struct uart16550 *uart16550 = (struct uart16550 *) file->private_data;
	int ret = 0;

	/* Wait until there is enough data */
	wait_event_interruptible(uart16550->wq_head, kfifo_len(&uart16550->read_buff));
	kfifo_to_user(&uart16550->read_buff, buf, size, &ret);

	return ret;
}

static ssize_t uart16550_write(struct file *file, const char __user *buf,
			       size_t size, loff_t *offset)
{
	struct uart16550 *uart16550 = (struct uart16550 *) file->private_data;
	u8 ier_reg = 0;
	int ret;

	/* Wait until data can be written */
	wait_event_interruptible(uart16550->wq_head, kfifo_avail(&uart16550->write_buff));
	kfifo_from_user(&uart16550->write_buff, buf, size, &ret);

	/* Enable Tx interrupt */
	ier_reg = inb(uart16550->base_addr + UART16550_IER);
	ier_reg |= (1 << UART16550_IER_xmit);
	outb(ier_reg, uart16550->base_addr + UART16550_IER);

	return ret;
}

static __always_inline void init_regs(struct uart16550_line_info *line_info, u16 base_addr)
{
	u8 ier_reg = 0, lcr_reg = 0;

	lcr_reg |= line_info->len;
	lcr_reg |= line_info->stop;
	lcr_reg |= line_info->par;
	lcr_reg |= (1 << UART16550_LCR_dlab);

	/* Initialize communication line. Access LCR, DLL, DLM registers */
	outb(lcr_reg, base_addr + UART16550_LCR);
	outb(line_info->baud, base_addr + UART16550_DLL);
	outb(0, base_addr + UART16550_DLM);

	/* Swap back RBR, THR, IER registers */
	lcr_reg &= ~(1 << UART16550_LCR_dlab);
	outb(lcr_reg, base_addr + UART16550_LCR);

	/* Enable Rx interrupt */
	ier_reg |= (1 << UART16550_IER_recv);
	outb(ier_reg, base_addr + UART16550_IER);
}

static long uart16550_ioctl(struct file *file, unsigned int cmd, unsigned long arg)
{
	struct uart16550 *uart16550 = (struct uart16550 *) file->private_data;
	struct uart16550_line_info line_info = { 0 };

	if (cmd != UART16550_IOCTL_SET_LINE)
		return -EINVAL;

	if (copy_from_user(&line_info, (struct uart16550_line_info *) arg,
			   sizeof(struct uart16550_line_info)))
		return -EFAULT;

	init_regs(&line_info, uart16550->base_addr);
	return 0;
}

const struct file_operations fops_com1 = {
	.open = uart16550_com1_open,
	.read = uart16550_read,
	.write = uart16550_write,
	.unlocked_ioctl = uart16550_ioctl,
};

const struct file_operations fops_com2 = {
	.open = uart16550_com2_open,
	.read = uart16550_read,
	.write = uart16550_write,
	.unlocked_ioctl = uart16550_ioctl,
};

const struct file_operations *fops[] = {
	&fops_com1,
	&fops_com2,
};

static irqreturn_t uart16550_irq_handler(int irq_no, void *dev)
{
	struct uart16550 *uart16550 = (struct uart16550 *) dev;
	u8 ier_reg = inb(uart16550->base_addr + UART16550_IER);
	u8 iir_reg = inb(uart16550->base_addr + UART16550_IIR);
	u8 data = 0;

	/* Tx */
	if (iir_reg & (1 << UART16550_IIR_xmit)) {
		if (!kfifo_len(&uart16550->write_buff)) {
			ier_reg &= ~(1 << UART16550_IER_xmit);
			outb(ier_reg, uart16550->base_addr + UART16550_IER);

			return IRQ_HANDLED;
		}

		kfifo_out(&uart16550->write_buff, &data, 1);
		outb(data, uart16550->base_addr + UART16550_THR);

		wake_up_interruptible(&uart16550->wq_head);
	}

	/* Rx */
	if ((iir_reg & (1 << UART16550_IIR_recv))) {
		data = inb(uart16550->base_addr + UART16550_RBR);
		kfifo_in(&uart16550->read_buff, &data, sizeof(data));

		wake_up_interruptible(&uart16550->wq_head);
	}

	return IRQ_HANDLED;
}

static int init_chrdev(int minor, char *dev_name, char *ioport_name,
		       int irq_line, irq_handler_t irq_handler)
{
	int err;

	err = register_chrdev_region(MKDEV(major, minor), 1, THIS_MODULE->name);
	if (err) {
		pr_err("register_chrdev_region(err = %d)\n", err);
		goto err_register;
	}

	cdev_init(&uart16550[minor].cdev, fops[minor]);
	err = cdev_add(&uart16550[minor].cdev, MKDEV(major, minor), 1);
	if (err) {
		pr_err("cdev_add(err = %d)\n", err);
		goto err_add;
	}

	uart16550[minor].device = device_create(class, NULL, MKDEV(major, minor), NULL, dev_name);
	if (IS_ERR(uart16550[minor].device)) {
		err = PTR_ERR(uart16550[minor].device);
		pr_err("device_create(err = %d)\n", err);
		goto err_device;
	}

	if (!request_region(uart16550[minor].base_addr, UART16550_NUM_REGS, ioport_name)) {
		err = -EBUSY;
		pr_err("request_region(err = %d)\n", err);
		goto err_region;
	}

	err = request_irq(irq_line, irq_handler, IRQF_SHARED, ioport_name,
			  (void *) &uart16550[minor]);
	if (err) {
		pr_err("request_irq(err = %d)\n", err);
		goto err_irq;
	}

	spin_lock_init(&uart16550->lock);
	INIT_KFIFO(uart16550[minor].read_buff);
	INIT_KFIFO(uart16550[minor].write_buff);
	init_waitqueue_head(&uart16550[minor].wq_head);
	return 0;

err_irq:
	free_irq(irq_line, &uart16550[minor]);
err_region:
	release_region(uart16550[minor].base_addr, UART16550_NUM_REGS);
err_device:
	device_destroy(class, MKDEV(major, minor));
err_add:
	cdev_del(&uart16550[minor].cdev);
err_register:
	unregister_chrdev_region(MKDEV(major, minor), 1);
	return err;
}

static int __init uart16550_init(void)
{
	int err;

	if (major > MAX_MAJOR)
		return -EINVAL;

	class = class_create(THIS_MODULE, THIS_MODULE->name);
	if (IS_ERR(class)) {
		err = PTR_ERR(class);
		pr_err("class_create(err = %d)\n", err);
		goto err_class;
	}

	switch (option) {
	case OPTION_BOTH:
		err = init_chrdev(UART16550_COM1_MINOR, "uart0", "uart16550",
				  UART16550_COM1_IRQ, uart16550_irq_handler);
		err = init_chrdev(UART16550_COM2_MINOR, "uart1", "uart16550",
				  UART16550_COM2_IRQ, uart16550_irq_handler);
		break;
	case OPTION_COM1:
		err = init_chrdev(UART16550_COM1_MINOR, "uart0", "uart16550",
				  UART16550_COM1_IRQ, uart16550_irq_handler);
		break;
	case OPTION_COM2:
		err = init_chrdev(UART16550_COM2_MINOR, "uart1", "uart16550",
				  UART16550_COM2_IRQ, uart16550_irq_handler);
		break;
	default:
		err = -EINVAL;
		break;
	}

	if (err)
		goto err_class;

	return 0;

err_class:
	class_destroy(class);
	return err;
}

static void exit_chrdev(int minor, int irq_line)
{
	free_irq(irq_line, &uart16550[minor]);
	release_region(uart16550[minor].base_addr, UART16550_NUM_REGS);
	device_destroy(class, MKDEV(major, minor));
	cdev_del(&uart16550[minor].cdev);
	unregister_chrdev_region(MKDEV(major, minor), 1);
}

static void __exit uart16550_exit(void)
{
	switch (option) {
	case OPTION_COM1:
		exit_chrdev(UART16550_COM1_MINOR, UART16550_COM1_IRQ);
		break;
	case OPTION_COM2:
		exit_chrdev(UART16550_COM2_MINOR, UART16550_COM2_IRQ);
		break;
	default:
		exit_chrdev(UART16550_COM1_MINOR, UART16550_COM1_IRQ);
		exit_chrdev(UART16550_COM2_MINOR, UART16550_COM2_IRQ);
		break;
	}

	class_destroy(class);
}

module_init(uart16550_init);
module_exit(uart16550_exit);

MODULE_DESCRIPTION("Kprobe-based tracer");
MODULE_AUTHOR("Robert-Ionut Alexa robertalexa2000@gmail.com");
MODULE_LICENSE("GPL v2");

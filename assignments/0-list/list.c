// SPDX-License-Identifier: GPL-2.0+

/*
 * list.c - Linux kernel list API
 *
 * Author: Robert-Ionut Alexa robertalexa2000@gmail.com
 */
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/list.h>
#include <linux/proc_fs.h>
#include <linux/seq_file.h>
#include <linux/uaccess.h>

#define PROCFS_MAX_SIZE		512
#define CMD_STRING_SIZE		5
#define CMD_OP_SIZE		3
#define CMD_OP_IDX		CMD_OP_SIZE

#define procfs_dir_name		"list"
#define procfs_file_read	"preview"
#define procfs_file_write	"management"

struct proc_dir_entry *proc_list;
struct proc_dir_entry *proc_list_read;
struct proc_dir_entry *proc_list_write;

struct node {
	struct list_head list;
	char buffer[PROCFS_MAX_SIZE];
	int len;
};

static struct list_head head;

static int list_proc_show(struct seq_file *m, void *v)
{
	struct node *node;
	struct list_head *curr;

	list_for_each(curr, &head) {
		node = container_of(curr, struct node, list);
		seq_printf(m, "%s", node->buffer);
	}

	return 0;
}

static int list_read_open(struct inode *inode, struct  file *file)
{
	return single_open(file, list_proc_show, NULL);
}

static int list_write_open(struct inode *inode, struct  file *file)
{
	return single_open(file, list_proc_show, NULL);
}

static ssize_t list_write(struct file *file, const char __user *buffer,
			  size_t count, loff_t *offs)
{
	char local_buffer[PROCFS_MAX_SIZE];
	char cmd_string[CMD_STRING_SIZE];
	unsigned long local_buffer_size = 0;
	struct node *node;
	struct list_head *curr, *tmp;

	local_buffer_size = count;
	if (local_buffer_size > PROCFS_MAX_SIZE)
		local_buffer_size = PROCFS_MAX_SIZE;

	memset(local_buffer, 0, PROCFS_MAX_SIZE);
	if (copy_from_user(local_buffer, buffer, local_buffer_size))
		return -EFAULT;

	/* extract the cmd to be executed */
	memcpy(cmd_string, local_buffer, CMD_STRING_SIZE - 1);
	cmd_string[CMD_STRING_SIZE - 1] = '\0';

	/* extract cmd input string */
	memcpy(local_buffer, local_buffer + CMD_STRING_SIZE,
	       local_buffer_size - CMD_STRING_SIZE + 1);

	if (!strncmp(cmd_string, "add", CMD_OP_SIZE)) {
		/* create a new node */
		node = kmalloc(sizeof(struct node), GFP_KERNEL);
		if (!node)
			return -ENOMEM;
		memcpy(node->buffer, local_buffer,
		       local_buffer_size - CMD_STRING_SIZE + 1);

		/* insert it into the list */
		if (cmd_string[CMD_OP_IDX] == 'f')
			list_add(&node->list, &head);
		else if (cmd_string[CMD_OP_IDX] == 'e')
			list_add(&node->list, head.prev);
		else
			return -EPERM;
	} else if (!strncmp(cmd_string, "del", CMD_OP_SIZE)) {
		if (cmd_string[CMD_OP_IDX] != 'f' && cmd_string[CMD_OP_IDX] != 'a')
			return -EPERM;

		/* delete the node from the list */
		list_for_each_safe(curr, tmp, &head) {
			node = container_of(curr, struct node, list);
			if (!strcmp(node->buffer, local_buffer)) {
				list_del(curr);
				kfree(node);

				if (cmd_string[CMD_OP_IDX] == 'f')
					break;
			}
		}
	} else
		return -EPERM;

	return local_buffer_size;
}

static const struct proc_ops r_pops = {
	.proc_open		= list_read_open,
	.proc_read		= seq_read,
	.proc_release		= single_release,
};

static const struct proc_ops w_pops = {
	.proc_open		= list_write_open,
	.proc_write		= list_write,
	.proc_release		= single_release,
};

static int list_init(void)
{
	INIT_LIST_HEAD(&head);

	proc_list = proc_mkdir(procfs_dir_name, NULL);
	if (!proc_list)
		return -ENOMEM;

	proc_list_read = proc_create(procfs_file_read, 0000, proc_list,
				     &r_pops);
	if (!proc_list_read)
		goto proc_list_cleanup;

	proc_list_write = proc_create(procfs_file_write, 0000, proc_list,
				      &w_pops);
	if (!proc_list_write)
		goto proc_list_read_cleanup;

	return 0;

proc_list_read_cleanup:
	proc_remove(proc_list_read);
proc_list_cleanup:
	proc_remove(proc_list);
	return -ENOMEM;
}

static void list_exit(void)
{
	struct list_head *curr, *tmp;
	struct node *node;

	list_for_each_safe(curr, tmp, &head) {
		node = container_of(curr, struct node, list);
		list_del(curr);
		kfree(node);
	}
	proc_remove(proc_list);
}

module_init(list_init);
module_exit(list_exit);

MODULE_DESCRIPTION("Linux kernel list API");
MODULE_AUTHOR("Robert-Ionut Alexa robertalexa2000@gmail.com");
MODULE_LICENSE("GPL v2");
